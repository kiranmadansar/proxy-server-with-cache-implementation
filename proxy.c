/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 10/20/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: TCP_server.c
 * Implements HTTP server 
 * usage: ./server <port>
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>
#include <openssl/md5.h>
#include <stdbool.h>

#define BUFSIZE (4000)
#define MAX_COUNT (1000)
#define CACHEEXPIRE (60)

/****************************************************************************************************
*
* Global Variable
*
****************************************************************************************************/
extern int h_errno;
int sockfd;

struct cache_host
{
	char name[BUFSIZE];
	char ip_address[BUFSIZE];
};

/* get host and ip address*/
bool get_Host(char *hostname, char *ip_addr)
{
	struct cache_host cache_struct;
	struct hostent *host_struct = gethostbyname(hostname);
	struct in_addr **addr_list;
	if(host_struct)
	{
		strcpy(cache_struct.name, hostname);
		addr_list = (struct in_addr **) host_struct->h_addr_list;
		strcpy(cache_struct.ip_address, inet_ntoa(*addr_list[0]));
		ip_addr = cache_struct.ip_address;
		FILE *fptr = fopen("cache_ip.txt", "r+");
		fwrite(&cache_struct, 1, sizeof(cache_struct), fptr);
		fclose(fptr);
		return 1;
	}
	else
	{
		FILE *fptr = fopen("cache_ip.txt", "r");
		while(!feof(fptr))
		{
			fread(&cache_struct, 1, sizeof(cache_struct), fptr);
			if(strcmp(cache_struct.name, hostname) == 0)
			{
				ip_addr = cache_struct.ip_address;
				return 1;
			}
		}
		memset(cache_struct.ip_address, '\0',BUFSIZE);
		fclose(fptr);
		return 0;
	}
}

void signal_handler()
{
	close(sockfd);
	printf("\nSignal Handler\n");
	exit(0);
}

void hostName(char* req_url, char* host_name) 
{
	sscanf(req_url, "http://%511[^/\n]", host_name);
}

int read_blocked(char *name, char *ip)
{
	char read_line[BUFSIZE];
	FILE *f = fopen("blacklist.txt", "r");
	while(!feof(f))
	{
		fgets(read_line, BUFSIZE, f);
		if(strstr(read_line, name) != NULL || strcmp(ip, read_line) == 0)
		{
			fclose(f);
			return 1;
		}
		
	}
	return 0;
}


void md5Sum(char *req_url, char *mdString)
{	
	// taken from https://stackoverflow.com/questions/3395690/md5sum-of-file-in-linux-c
	unsigned char str[16];
	MD5_CTX ctx;
	MD5_Init(&ctx);
	MD5_Update(&ctx, req_url, strlen(req_url));
	MD5_Final(str, &ctx);
	for (int i = 0; i < 16; i++)
		sprintf((mdString+i*2), "%02x", (unsigned int)str[i]);
}


FILE * checkCache(char *req_url)
{
	char mdString[64];
	bzero(mdString, 64);
	char cache_root[BUFSIZE] = "/home/kiranhegde/Downloads/PA3/cache/";
	md5Sum(req_url, mdString);
	strcat(mdString, ".txt");
	strcat(cache_root, mdString);
	FILE *fptr = fopen(cache_root, "r");
	if(!fptr)
	{
		printf("[ERROR] File Not found in cache\n");
		return NULL;
	}
	else
	{	char time_str[BUFSIZE];
		fgets(time_str, BUFSIZE, fptr);
		time_t old;
		sscanf(time_str, "%ld", &old);
		time_t now = time(NULL);
		if(difftime(now, old) > CACHEEXPIRE)	
		{
			printf("[ERROR] Cache expired\n");
			return NULL;
		}
		else
		{
			printf("[SUCCESS] File found in cache\n");
			return fptr;
		}
	}
}

int handleNonCached(int client_sock, char *req_link, char *req_version, char *host_name, char *req_url)
{
	char cache_root[BUFSIZE] = "/home/kiranhegde/Downloads/PA3/cache/";
	int ser_sock; 
	long int recv_bytes;
	char mdString[64];
	bzero(mdString, 64);
	md5Sum(req_url, mdString);
	char recv_buffer[BUFSIZE], buf_send[BUFSIZE];
	struct sockaddr_in server;
	ser_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (ser_sock < 0)
	{ 
		printf("ERROR opening socket");
		return 1;
	}
	int opt=1;
	setsockopt(ser_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt));
	struct hostent *host_struct;
	host_struct = gethostbyname(host_name);
	struct in_addr **addr;
	addr = (struct in_addr **)host_struct->h_addr_list;
	server.sin_family = AF_INET;
	memcpy(&server.sin_addr, host_struct->h_addr_list[0], host_struct->h_length);
	server.sin_port = htons(80);
	socklen_t server_size = sizeof(server);
	int ser_conn = connect(ser_sock, (struct sockaddr *) &server, server_size);
	if(ser_conn < 0)
	{
		printf("[ERROR] Server connection failed\n");
		close(client_sock);
		return 1;
	}
	
	strcat(mdString, ".txt");
	strcat(cache_root, mdString);
	FILE *f_cache = fopen(cache_root, "w");
	if(!f_cache)
	{
		printf("[ERROR] File can't be opened for cache write");
		return 1;
	}
	fprintf(f_cache, "%ld\n", time(NULL));
	sprintf(buf_send, "GET %s %s\r\nHost: %s\r\nConnection: close\r\n\r\n", req_link, req_version, host_name);
	send(ser_sock, buf_send, strlen(buf_send), 0);
	do{
		bzero(recv_buffer,BUFSIZE);
		recv_bytes=recv(ser_sock,recv_buffer,BUFSIZE,0);
        	if(recv_bytes)
		{
        		send(client_sock,recv_buffer,recv_bytes,0);
			fwrite(recv_buffer, 1, recv_bytes, f_cache);		
		}
        	//write this data to cache
	}while(recv_bytes>0);
	//fclose(f_cache);
	printf("[SUCCESS] Content Sent to client\n");
	shutdown(ser_sock,SHUT_RDWR);
	close(ser_sock);

}
/****************************************************************************************************
*
* @name fork_function
* @brief Handles the data sending in child process 
*
* After a request has been made by the client, the server creates a child process to handle it and 
* calls this function. This handles all data transfer  
*
* @param int client_sock client socket 
*
* @return 0 on No error
* @return 1 on error
*
****************************************************************************************************/
unsigned int fork_function(int client_sock)
{
	FILE *file_ptr;
	int  n, file_size, read_size;
	char buf[BUFSIZE], type[25], req_version[BUFSIZE], req_method[BUFSIZE], req_url[BUFSIZE], req_link[BUFSIZE];
	char *url_ptr;
	bzero(req_url, BUFSIZE);
	char *ptr, request[4000];
	char header[4000], host_name[BUFSIZE], err_msg[BUFSIZE];
	struct hostent *host_struct;

	memset(req_url, 0, BUFSIZE);
	memset(req_method, 0, BUFSIZE);
	/* receive the data from client */
	if(recv(client_sock, buf, BUFSIZE, 0) < 0)
	{
		printf("[ERROR] Cound Not Receive from Client\n");
		return 1;
	}
	sscanf(buf, "%s %s %s", req_method, req_url, req_version);
	if(strcmp(req_method, "GET") != 0)
	{
		/* if bad request, send back the error */
		sprintf(err_msg, "<html><body>ERROR 400 BAD REQUEST: %s</body></html>\r\n\r\n", req_method);
		sprintf(header, "HTTP/1.0 ERROR 400 BAD REQUEST\r\nContent-Type: text/html\r\nContent-Length: %lu\r\n\r\n%s", strlen(err_msg), err_msg);
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] Bad Request %s\n", req_method);
		return 1;
	}
	if((strcmp(req_version, "HTTP/1.1")!=0) && (strcmp(req_version, "HTTP/1.0")!=0))
	{
		/* if bad version, send back the error */
		sprintf(err_msg, "<html><body>ERROR 400 BAD REQUEST: %s</body></html>\r\n\r\n", req_version);
		sprintf(header, "HTTP/1.0 ERROR 400 BAD REQUEST\r\nContent-Type: text/html\r\nContent-Length: %lu\r\n\r\n%s", strlen(err_msg), err_msg);
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] Bad Version %s\n", req_version);
		return 1;
	}

	hostName(req_url, host_name);
	char ip_addr[BUFSIZE];
	bool status = get_Host(host_name, ip_addr);  
	if(!status)
	{
		/* if bad host name, send back the error */
		sprintf(err_msg, "<html><body>ERROR 400 BAD REQUEST: %s</body></html>\r\n\r\n", req_url);
		sprintf(header, "HTTP/1.0 ERROR 400 BAD REQUEST\r\nContent-Type: text/html\r\nContent-Length: %lu\r\n\r\n%s", strlen(err_msg), err_msg);
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] Bad Host Name %s\n", req_url);
		return 1;
	}
	
	/* handle blocked list */
	if(read_blocked(host_name, ip_addr))
	{
		sprintf(err_msg, "<html><body>ERROR 403 Forbidden: %s</body></html>\r\n\r\n", req_url);
		sprintf(header, "HTTP/1.0 ERROR 403 Forbidden\r\nContent-Type: text/html\r\nContent-Length: %lu\r\n\r\n%s", strlen(err_msg), err_msg);
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] URL Blocked %s\n", req_url);
		return 1;
	}
	
	url_ptr = req_url;
	url_ptr = url_ptr+7;
	char *temp = strchr(url_ptr, '/');
	if(temp)
		strncpy(req_link, temp, strlen(temp));
	else
		strcpy(req_link, "/");

	FILE *f_cache = checkCache(req_url);
	if(f_cache)
	{
		int read_size;
		char buff[BUFSIZE];
		fseek(f_cache, 0L, SEEK_END);
		int file_size = ftell(f_cache);
		rewind(f_cache);
		fgets(buff, BUFSIZE, f_cache);
		sprintf(header, "%s 200 OK\r\nContent-Type: %s\r\nContent-Length: %u\r\n\r\n", req_version, type, file_size);
		bzero(buff, BUFSIZE);
		/* send whole file */
		while((read_size = fread(buff, 1, BUFSIZE, f_cache))>0)
		{
			send(client_sock, buff, read_size, 0);
			bzero(buff, BUFSIZE);
		}
		fclose(f_cache);
		printf("********************************\n[SUCCESS] Content Sent From Cache\n******************************\n");
		
	}
	else
	{	
		handleNonCached(client_sock, req_link, req_version, host_name, req_url);
	}
	/* return 0 on no error*/
	return 0;
}


/****************************************************************************************************
*
* @name Main
* @brief Implements the server functionality 
*
* This is the main function where all the server functionalities are implemented. 
*
* @param argc - argument count 
* @param **argv - pointer to argument  
*
* @return 0 on No error
*
****************************************************************************************************/
int main(int argc, char **argv) 
{
	int client_sock; /* socket */
	int portno; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int ret, count = 0; /* message byte size */
	int pid;

	system("rm /home/kiranhegde/Downloads/PA3/cache/*.txt");
	/* 
	* check command line arguments 
	*/
	if (argc != 2) 
	{
    		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(1);
	}
	portno = atoi(argv[1]);

	/* 
	* socket: create the parent socket 
	*/
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		printf("ERROR opening socket");
	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt));
	/*
	* build the server's Internet address
	*/
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);

	/* 
	* bind: associate the parent socket with a port 
	*/
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
		printf("ERROR on binding");

	/* 
	* main loop: wait for a datagram, then echo it
	*/
	listen(sockfd, 500);
	clientlen = sizeof(serveraddr);
	signal(SIGINT, signal_handler);
	while(1)
	{
		if((client_sock = accept(sockfd, (struct sockaddr *)&serveraddr, (socklen_t *)&clientlen)) < 0)
		{
			printf("Accept failed\n");
			return 1;
		}
		pid = fork();
		if (pid<0) return -1;
		else if (pid == 0)
		{
			close(sockfd);
			ret = fork_function(client_sock);
		        shutdown(client_sock, SHUT_RDWR);
			close(client_sock);
			exit(0);	
		}

	}
	return 0;
}
