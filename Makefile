CC=gcc

.PHONY: all
all:
	$(CC) proxy.c -lcrypto -lssl -o proxy

.PHONY: clean
clean:
	rm -f proxy
