﻿Date: 11/20/2018
Programming Assignment 3 (CSCI 5273) - Proxy server with cache Programming
Submitted by Kiran Narayana Hegde


I have implemented a proxy server which supports multiple client connection. I have used fork() to create child process.


It supports version HTTP/1.1 and HTTP/1.0.  Only “GET" requests are supported by the server. 


Webpages or IP address can be blocked by mentioning them in blacklist.txt

Cache mechanism is implemented with timout to improve the performance.


How to build and use the application ?


1. There is a directory named ‘Proxy_Server’ in the submitted .tar file. Untar it
2. Go to directory and use the following make command to build client application
3. In terminal, run the server application using the following commands

```sh
$ make
$ ./proxy <port_no>
```

4. Configure browser for proxy server with proper port_no. And browse your favourite site. 
